#!/usr/bin/python3
from sys import stderr

def main(n, bits):
    if len(n) > bits:
        stderr.write("{} ne peux être écrit sur {} bits\n".format(n, bits))
    elif len(n) == bits:
        return n
    else:
        return '1' * (bits-len(n)) + n
