#!/usr/bin/python3
import time
import getpass

l = []
t1 = time.time()

for i in range(0,200000000):
    l.append(i**2)

duration = time.time() - t1
print('Votre ordinateur à pris {} secondes pour calculer chaque carré des nombres entre 0 et 200 000 000'.format(duration))

while i not in ['y', 'n']:
    i = input('Voulez vous enregistrer ce score ? y/n ')

    if i not in ['y', 'n']:
        print("\nErreur : Les seules options disponible sont 'y' et 'n'\n")

if i == 'y':
    file = open("./scores", mode='a')
    file.write(getpass.getuser() + ' : ' + str(duration) + ' seconde(s)\n')
    file.close()

