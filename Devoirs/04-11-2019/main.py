#!/usr/bin/python3
from random import randint

def play():
    s1 = 0
    s2 = 0

    while (s1 < 10) + (s2 < 10) == 2:
        n1 = randint(0, 100)
        n2 = randint(0, 100)

        if n1 > n2:
            s1 += 1
        else:
            s2 += 1

    print("Score")
    print("..... J1 :", s1)
    print("..... J2 :", s2)

play()

