#!/usr/bin/python3
import random

def ex1():
    """
    Instructions de l'ex 1
    """

    L1 = [4,8,2,7]
    print(L1[1])

    L2 = []

    for i in [4,8,2,7]:
        L2.append(i)

    L3 = [2*i**3 for i in range(7)]
    
    del(L3[0])
    L2.remove(7)

    for i in L1:
        print(i)

    for i in range(len(L1)):
        print(L1[i])

def calcul_moyenne(tab):
    x = 0
    for i in tab:
        x += i
    return x/len(tab)

def detecter_suite_geometrique(tab):
    try:
        coeff = tab[1] / tab[0]
        i     = 1
        r     = True
        while i < (len(tab) - 1) and r == True:
            if tab[i+1]/tab[i] != coeff:
                r = False
            i += 1
        return r
    except ZeroDivisionError:
        return False

def detecter_suite_geometrique_app(tab):
    try:
        coeff = tab[1] / tab[0]
        i     = 1
        r     = True
        while i < (len(tab) - 1) and r == True:
            if abs(tab[i+1]/tab[i] - coeff) > 1:
                r = False
            i += 1
        return r
    except ZeroDivisionError:
        return False

def tableau_alea_croissant(n):
    r = [random.randint(0,100)]
    for i in range(n):
        r.append(r[-1] + random.randint(0,100))
    return r
