#!/usr/bin/python3
from zazac import *
#import pygame
import matplotlib.pyplot as plt

def GUIPoids():
    m = float(input("m = "))
    g = float(input("g = "))
    pygame.init()

    poids = Poids(m, g)

    font    = pygame.font.SysFont('Source Code Pro', 15)
    fenetre = pygame.display.set_mode((400, 400))
    fond    = pygame.image.load("images/fond_800x800.png")
    cercle  = pygame.image.load("images/rond_bleu_200px.png")
    fleche  = pygame.image.load("images/flèche_rouge_200px.png")
    pygame.display.flip()

    on = True
    while on:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                on = False
            elif event.type == pygame.KEYDOWN:
                if event.key == 27:
                    print("évasion...")
                    on = False

        fenetre.blit(fond, (0,0))
        fenetre.blit(cercle, (75, 100))
        fenetre.blit(fleche, (225, 100))
        fenetre.blit(font.render(Prefixe(poids) + 'N', False, (255, 255, 255)), (287, 100)) # Affichage du poids
        fenetre.blit(font.render('g = ' + str(g) + ' m/s', False, (255, 255, 255)), (10, 10)) # Affichage de g
        fenetre.blit(font.render('m = ' + str(m) + ' kg', False, (255, 255, 255)), (10, 23)) # Affichage de g
        pygame.display.flip()

def GUIvitesse_acceleration_lignedroite():
    h          = float(input('h = '))
    g          = float(input('g = '))
    a, v, y, t = vitesse_acceleration_lignedroite(h,g)
    
    plt.plot(t, y)

    plt.xlabel("Temps (en s)")
    plt.ylabel("Position y (en m)")
    plt.grid()

    plt.show()

functions = {
#    '1':GUIPoids,
    '1':GUIvitesse_acceleration_lignedroite
}

print("Sélectionner une fonction")
#print("1 : Poids")
print("1 : Vitesse Acceleration Ligne Droite")

i = input(">")

try:
    functions[i]()
except KeyError:
    print("Cette fonction n'existe pas, évasion")
