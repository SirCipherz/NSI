def Acceleration(F, m):
    """
    F : Force
    m : masse
    """
    return F/m

def ForceGravitationelle(m1, m2, d, G=6.67*10**-11):
    """
    m1 : Masse de 1
    m2 : Masse de 2
    d  : Distance entre 1 et 2
    """
    return G*((m1*m2)/d**2)

def Poids(m, g):
    """
    m : masse
    g : pesanteur
    """
    return m*g


def Prefixe(n):
    if n < 10**3:
        return '%0.2f' % n
    elif n < 10**6:
        return '%0.2f' % n + ' k'
    elif n < 10**9:
        return '%0.2f' % n + ' M'
    elif n < 10**12:
        return '%0.2f' % n + ' G'
    elif n < 10**15:
        return '%0.2f' % n + ' T'
    else:
        return '%0.2f' % n

def vitesse_acceleration_lignedroite(h, g, delta_t=1*10**-3):
    a = [-g]
    v = [0]
    y = [h]
    t = [0]
    i = 1
    while y[-1] > 0:
        a.append(-g)
        y.append(y[-1]+ v[-1]*delta_t)
        v.append(v[-1]+a[-1]*delta_t)
        t.append(i*delta_t)
        i += 1

    return a,v,y,t



