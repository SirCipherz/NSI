#!/usr/bin/python3
from math import ceil

def donner_element(liste, indice):
    if indice >= 0 and indice <= len(liste):
        return liste[indice]
    else:
        return None

def Affichage_Contenu_Liste(liste):
    for i in liste:
        print(i)

def Compter_Apparition(liste, char):
    n = 0
    for i in liste:
        if i == char:
            n += 1
    return n

def Est_Symetrique(liste):
    symetrique = True
    i          = 0
    while symetrique == True and i < ceil(len(liste)/2):
        if liste[i] != liste[-(i+1)]:
            symetrique = False
        i += 1
    return symetrique

def Moyenne(liste):
    return sum(liste)/len(liste)

def Ecart_Type(liste):
    return (moy)**0.5

def Minimum(liste):
    return sorted(liste)[0]

def Maximum(liste):
    return sorted(liste)[-1]
