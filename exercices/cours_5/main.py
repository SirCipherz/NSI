#!/usr/bin/python3

def rechercher_caractere(chaine, char):
    for i in chaine:
        if i == char:
            return True
    return False

def compter_caractere(chaine, char):
    i = 0
    for j in chaine:
        if j == char:
            i += 1
    return i

def rot(input, n):
    output = ""
    string = []

    for i in input:
        if ord(i) + n > 122:
            string.append(ord(i) + n - 26)
        else:
            string.append(ord(i) + n)

    for i in string:
        output += chr(i)

    return output

def char_freq(string):
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    n        = []

    for i in range(len(alphabet)):
        n.append(0)

    for i in string:
        n[ord(i) - 97] += 1

    max        = 0
    indicemaxe = 0

    for i in range(len(n)):
        if n[i] > max:
            indicemax = i
    
    return alphabet[indicemax]

