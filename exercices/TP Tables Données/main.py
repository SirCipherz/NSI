#!/usr/bin/python3
import csv

def ex1():
    with open("donnees/prenoms.txt") as fichier:
        prenoms = fichier.readlines()

    print(prenoms)

def donner_prenoms_courts(tab, n):
    """
    Entrée : liste, taille max du prénom
    Sortie : Prénom de moins de n caractères
    """
    new_tab = []

    for i in tab:
        if len(i) <= n:
            new_tab.append(i)

    return new_tab

def ex3():
    with open("donnees/nom_fichier.txt", "w") as fichier:
        fichier.write("24\n")

def donner_meilleurs_joueurs(n):
    with open("donnees/joueurs.csv") as fichier:
        joueurs = list(csv.reader(fichier))
    new_table = []
    for i in range(1,len(joueurs[0])-1):
        if int(joueurs[i][2]) >= n:
            new_table.append(joueurs[i])
    return new_table

def donner_meilleurs_joueurs_dict(n):
    with open("donnees/joueurs.csv") as fichier:
        joueurs = list(csv.DictReader(fichier))
    new_table = []
    for i in joueurs:
        if int(i["score"]) > n:
            new_table.append(i)
    return new_table

# Tests

#Ex1
ex1()

# Ex2
print(donner_prenoms_courts(["Sam", "Tom", "Titi"], 3))

# Ex3
ex3()

#Ex II 1
tmp = donner_meilleurs_joueurs(100)
print(tmp)

#Ex II 2
tmp = donner_meilleurs_joueurs_dict(100)
print(tmp)
