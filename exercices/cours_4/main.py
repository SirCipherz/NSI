#!/usr/bin/python3

def cp1(n):
    """
    Effectue le complément à 1
    """
    r = ''
    for i in range(1, len(n)+1):
        if n[-i] == "0":
            r = "1" + r
        else:
            r = "0" + r

    return r

def formaterBinairePositif(n, bits):
    if len(n) == bits:
        r = n
    elif len(n) > bits:
        print("{} ne peut être encodé sur {} bits".format(n, bits))
    else:
        r = (bits - len(n)) * "0" + n

    return r

def binToDec(n):
    r     = 0 # Variable de retour
    i     = len(n)
    p     = 0
    while i > 0:
        i -= 1
        r += int(n[i])*2**p
        p += 1
    return r

def decToBin(n):
    r = "" # Variable de retour
    while n > 0:
        r = str(n%2) + r # Ajoute 0 ou 1 avant r
        n = n//2

    return r

def additionBinaire(n1, n2):
    i       = len(n1) - 1
    r       = ''
    retenue = 0
    while i >= 0:
        s       = int(n1[i]) + int(n2[i]) + retenue
        r       = str(s%2) + r
        retenue = s//2
        i -= 1

    return r

def cp2(n):
    return additionBinaire(cp1(n), '0'*(len(n)-1) + '1')

def bin2decS(n):
    if n[0] == '0':
        return binToDec(n)
    else:
        return - binToDec(cp2(n))
