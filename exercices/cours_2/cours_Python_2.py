#-*- coding:utf-8 -*-

"""
   Langage python
   Fiche 2 : structure conditionnelle, boucles bornées et non bornées, gestion d'exception
"""

# ****************************************************************************
# Structure conditionnelle
# ****************************************************************************

a = 9
message = "gagner"
b = 2
print("\na :", a)
print("message :", message, "\n")


if a == 15 :      #  (double égalité pour test d’égalité !)
    print("a est bien égal à 15\n")


if a <= 10  and  message == "gagner" :
    print("a<=10 et message est égal à 'gagner'\n") 
else :
    print("a n'est pas <=10   ou   message n'est pas égal à 'gagner'   ou   aucune des deux conditions n'est vérifiée\n")

    
if a > 15 : 
    print("a > 15 \n")
elif a > 5 :
    print("a n'est pas >15  et  a >5 \n")
else :
    print("a n'est pas >15  et  a n'est pas >5 \n") 



# ****************************************************************************
# Boucle conditionnelle (boucle non bornée)
# ****************************************************************************

nb_vie = 3
compteur = 0
continuer = True
n = 0

while nb_vie > 0 :
   print("nb_vie :", nb_vie)
   nb_vie = nb_vie - 1

print("\n")

while compteur < 12  and  continuer == True :
    print("compteur :", compteur)
    compteur = compteur + 1

print("\n")

while True:
    print("compteur :", compteur)
    if compteur == 20 :
        break
    compteur = compteur + 1

print("\n")

while n < 10 :
    n = n + 1       # (ou n += 1)
    if n == 6 :
        continue
    print("n :", n)

print("\n")    
n = 0

while n < 10 :
    n = n + 1      #  (ou n += 1)
    if n == 6 :
        break
    print("n :", n)


# ****************************************************************************
# Boucle itérative (boucle bornée)
# ****************************************************************************
print("\n")


# Attention, la borne de droite est exclue !

for i in range(0 , 4) :
    print("i :", i)


print("\n")


for i in range(4) :
    print("i :", i)


print("\n")


for i in range(0 , 5 , 2) :
    print("i :", i)


print("\n")


for i in range(0,5):
    if i == 3 :
        continue    
    print("i :", i)


print("\n")


for i in range(0,5):
    if i == 3 :
        break      
    print("i :", i)


print("\n")


# ****************************************************************************
# Gestion d'une exception
# ****************************************************************************

for i in range(-3,4) :
    try :
        print("1 /", i, ":", 1 / i)
    except :
        print("Attention : division par zero !")


