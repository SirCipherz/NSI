
                                                # ********************
                                                #    module_images_pgm
                                                # ********************

# *************************************************************************
def image_pgm_donne_tableau(nom_image) :
# *************************************************************************
    """
    Cette fonction place les valeur de pixels d'une image en niveaux de gris
    dans un tableau à deux dimensions
    0 : noir     255 : blanc    valeur intermédiaire : gris
    """
    import os
    tab = []
    nom = "images/" + nom_image

    fichier = open(nom,"r", encoding="utf8")
    
     # On place le texte dans une liste (un élément par ligne)
    liste_texte = fichier.readlines()

    texte = ""
    n = 0

    # Entete
    nb_magique = liste_texte[0].replace("\n", "")
    if nb_magique == "P2" :
        for ligne in liste_texte :
            ligne = ligne.replace("\n", " ")
            if n == 2 :
                [largeur , hauteur] = ligne.split()
                [largeur , hauteur] = [int(largeur) , int(hauteur)]
            if n == 3 :
                maxi = int(ligne)

    # La valeurs de pixels sont placées dans une chine de caractères
    # séparées par un espace
            elif n > 3 :
                texte = texte + ligne + " "
            n = n+ 1
    else :
        print("Il ne s'agit pas d'un format pgm")

    # Les valeurs de pixels sont placées dans un tableau
    n = 0
    ligne = []
    pixels = texte.split()      # Liste contenant les valeurs de pixels
    for carac in pixels :
        ligne.append(int(carac))
        n = n + 1
        if n == largeur :                           # Passage à la ligne suivante
            n = 0
            tab.append(ligne)
            ligne = []
        
    # fermeture du fichier
    fichier.close()

    return tab


# *************************************************************************
def tableau_donne_image_pgm(tab , nom_image="resultat.pgm") :
# *************************************************************************
    """
    Cette fonction place les valeur de pixels contenues un tableau à deux dimensions
    dans d'une image en niveaux de gris (pgm)
    0 : noir     255 : blanc    valeur intermédiaire : gris
    """
    import os

    nom = "images/" + nom_image
    fichier = open(nom,"w", encoding="utf8")

    # Entete
    fichier.write("P2\n")
    fichier.write("# Created by Python3\n")
    [hauteur , largeur] = [len(tab) , len(tab[0])]
    fichier.write(str(largeur) + " " + str(hauteur) + "\n")
    fichier.write("255\n")

    # Valeurs de pixels
    for i in range(hauteur) :
        for j in range(largeur) :
            px = str(tab[i][j])
            if len(px) == 1 :
                fichier.write(" " + px + " ")
            elif len(px) == 2 :
                fichier.write(" " + px)
            else :
                fichier.write(px)
            fichier.write(" ")
        fichier.write("\n")

    # fermeture du fichier
    fichier.close()


# *************************************************************************
def reformater_pgm(nom_image) :
# *************************************************************************
    """
    Cette fonction reformate le fichier pgm
    """
    tableau_donne_image_pgm(image_pgm_donne_tableau(nom_image), "formatage_" + nom_image)