#!/usr/bin/python3
from truc_du_prof import *

def compter_apparition_2(tab, valeur):
    """
    Compte les apparitions d'une valeur dans une liste
    Entrée : tableau, valeur
    Sortie : nombre entier
    """
    x = 0 # Initialise le compteur
    for i in tab: # Parcours le tableau ligne par ligne
        for j in i: # Parcours la ligne case par case
            if j == valeur: # Si la valeur de la case est égale à la valeur recherchée
                x += 1 # Incrémentation du compteur
    return x

def minimum_2(tab):
    """
    Renvoie la plus petite valeur d'un tableau
    Entrée : tableau
    Sortie : nombre
    """
    min = tab[0][0] # Applique la première valeur du tableau en tant que minimum
    for i in tab: # Parcours la tableau ligne par ligne
        for j in i: # Parcours la ligne case par case
            if j < min: # Si la valeur de la case est inférieur à la valeur minimum
                min = j # Si oui applique la nouvelle valeur
    return min

def niveaux_gris_vers_BnW(img):
    """
    Convertis une image en niveau de gris vers une image en noir et blanc
    Entrée : tableau
    Sortie : tableau
    """
    after = img.copy() # Fait une copie de l'image
    for y in range(len(img)): # parcours l'image ligne par ligne
        for x in range(len(img[y])): # parcours tout les pixels de la ligne
            if img[y][x] > 128: # Vérifie si la couleur est suppérieure à 128
                after[y][x] = 255 # Applique la couleur blanche
            else: # Sinon
                after[y][x] = 0 # Applique la couleur noire
    return after

def symetrie(img):
    """
    Fait la symétrie d'une image
    Entrée : tableau
    Sortie : tableau
    """
    after = img.copy() # Fait une copie de l'image
    for y in range(len(img)): # Parcours l'image ligne par ligne
        img[y].reverse() # Inverse la ligne
        after[y] = img[y] # Applique la modification
    return after # retourne l'image

def flouter(img):
    """
    Floute une image
    Entrée : tableau
    Sortie : tableau
    """
    after = img.copy() # Fait une copie de l'image
    for i in range(len(after[0])/3):
        step = [] # Initialise l'étape
        after[i*3] = 

"""
TESTS
"""

simple  = [[1,2,3], [4,5,6], [7,8,9]]
simple2 = [[6,2,8], [7,1,2], [9,9,0]]
img1    = [[17,200,8], [180,7,4], [15,18,8]]

#print(compter_apparition_2(simple, 1))a
#print(minimum_2(simple2))
#print(niveaux_gris_vers_BnW(img1))
#print(symetrie(img1))
#tableau_donne_image_pgm((symetrie(image_pgm_donne_tableau("pergaud.pgm"))))
