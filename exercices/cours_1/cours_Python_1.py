#!/usr/bin/python3

"""
   Langage python
   Fiche 1 : variables, affichage et saisie, calculs
"""

# ****************************************************************************
# Variables
# ****************************************************************************
print("\n")     # affichage d'un saut de ligne

nom_var = 15
nom_var_1 , nom_var_2 = 13 , 0.8

taille =  1.75
age = 16
message_3 = "coordonnées du point"
jeu_en_cours = False

print("taille :", taille)
print("age :", age)
print("message_3 :", message_3)
print("jeu_en_cours :", jeu_en_cours, "\n")     # ajout de "\n" pour un saut de ligne

print("Type de la variable taille :", type(taille))
print("Type de la variable age :", type(age))
print("Type de la variable message_3 :", type(message_3))
print("Type de la variable jeu_en_cours :", type(jeu_en_cours), "\n")

if isinstance(age , int) :
    print("La variable age est un entier\n")
# (ou test des types float , str , bool)


print("int(taille) :", int(taille), "\n")
# ou    float(…)   ,   str(…)    ,  bool(…)



# ****************************************************************************
# Affichage et saisie
# ****************************************************************************

print("age :" , age , end=" ")
print("jeu_en_cours :" , jeu_en_cours, "\n")

nom = input("Saisir la valeur de nom : ")
age = int(input("Saisir la valeur de l’âge (entier) : "))
d = float(input("Saisir la valeur de la distance (réel ou entier) : "))

print("\nnom :", nom)
print("age :" , age)
print("d :", d, "\n")


# ****************************************************************************
# Calculs
# ****************************************************************************

print("round(taille , 1) :", round(taille , 1))
print("6380e3, 5.14e-7, 0.012E6 :", 6380e3, 5.14e-7, 0.012E6, "\n")

print("19 // 5 :", 19 // 5 )
print("19 % 5 :", 19 % 5 )
print("2**3 :", 2**3, "\n")

longueur = 124.354848
affichage = "{0:.2e}" .format(longueur)
print(longueur, ":", affichage)
affichage = "{0:.3e}" .format(longueur)
print(longueur, ":", affichage, "\n")

import math
print("math.log10(8.2e-3) :", math.log10(8.2e-3))
print("math.log(8.2e-3):", math.log(8.2e-3))
print("math.sin(math.pi/2) :", math.sin(math.pi/2))
print("math.radians(180) :", math.radians(180) )
print("math.degrees(math.pi/4) :", math.degrees(math.pi/4))
print("math.sqrt(16) :", math.sqrt(16))
print("math.asin(math.sin(1)) :", math.asin(math.sin(1)), "\n")

import random
print("random.randint(0 , 7) :", random.randint(0 , 7))
print("random.random() :", random.random(), "\n")


