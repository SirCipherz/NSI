#!/usr/bin/python3
from math import cos, sin, radians

hypo  = int(input("Longeur de l'hypothénuse : "))
angle = int(input("Valeur de l'angle        : "))

AB = hypo * cos(radians(angle))
BC = hypo * sin(radians(angle))

print("AB : {}\nBC : {}".format(AB, BC))
