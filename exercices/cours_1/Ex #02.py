#!/usr/bin/python3

name  = input("Nom                : ")
birth = int(input("Année de naissance : "))

print("Vous vous nommez {} et vous êtes né en {}".format(name, birth))
print("Vous serez majeur en", birth + 18)
