#!/usr/bin/python3
import csv

def ex1():
    with open("donnees/repertoire.csv") as fichier:
        repertoire = list(csv.DictReader(fichier))
    with open("donnees/villes.csv") as fichier:
        villes = list(csv.DictReader(fichier))
    return repertoire, villes

def hab(x):
    return int(x["population"])

def ex2():
    with open("donnees/villes.csv") as fichier:
        villes = list(csv.DictReader(fichier))
    triee  = sorted(villes, key=hab, reverse=True)
    entete = ['f','departement','nom','code_commune','population','densite','surface','longitude','latitude','zmin','zmax']
    with open("ville_tri_pop_inv.csv", "w") as fichier:
        w = csv.DictWriter(fichier, fieldnames=entete,delimiter=",",lineterminator='\n')
        w.writeheader()
        w.writerows(triee)

def nom(x):
    return x["nom"]

def ex3():
    with open("donnees/repertoire.csv") as fichier:
        repertoire = list(csv.DictReader(fichier))
    triee  = sorted(repertoire, key=nom)
    entete = ['id','prenom','nom','date_naissance','ville','num_tel']
    with open("repertoire_tri_nom_inv.csv", "w") as fichier:
        w = csv.DictWriter(fichier, fieldnames=entete,delimiter=",",lineterminator='\n')
        w.writeheader()
        w.writerows(triee)

def tri2(x):
    return x["ville"], x["nom"]

def ex4():
    with open("donnees/repertoire.csv") as fichier:
        repertoire = list(csv.DictReader(fichier))
    triee  = sorted(repertoire, key=tri2)
    entete = ['id','prenom','nom','date_naissance','ville','num_tel']
    with open("repertoire_tri_nom_inv.csv", "w") as fichier:
        w = csv.DictWriter(fichier, fieldnames=entete,delimiter=",",lineterminator='\n')
        w.writeheader()
        w.writerows(triee)

