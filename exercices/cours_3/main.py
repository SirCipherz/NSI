#!/usr/bin/python3

def decToBin(n):
    """
    Cette focntion prend comme argument d'entré un nombre et retourne le même nombre en base 2
    """
    r = "" # Variable de retour
    while n > 0:
        r = str(n%2) + r # Ajoute 0 ou 1 avant r
        n = n//2

    return r

def binToDec(n):
    """
    Cette fonction prend comme argument d'entré une chaine de caractère d'un nombre en base 2 et retourne le même nombre en base 10
    """
    r     = 0 # Variable de retour
    i     = len(n)
    p     = 0
    while i > 0:
        i -= 1
        r += int(n[i])*2**p
        p += 1
    return r

def decToHex(n):
    r      = ""
    chars  = {0:'0', 1:'1', 2:'2', 3:'3', 4:'4', 5:'5', 6:'6', 7:'7', 8:'8', 9:'9', 10:'a', 11:'b', 12:'c', 13:'d', 14:'e', 15:'f'}
    while n > 0:
        r = chars[n%16] + r
        n = n//16
    return r

def hexToDec(n):
    chars = {'0':0, '1':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, 'a':10, 'b':11, 'c':12, 'd':13, 'e':14, 'f':15}
    r = 0
    i = len(n)
    p = 0
    while i > 0:
        i -= 1
        r += chars[n[i]]*16**p
        p += 1
    return r

def hexToBin(n):
    return decToBin(hexToDec(n))

def binAddition(a, b):
    return decToBin(binToDec(a) + binToDec(b))

def decToShadok(n):
    r      = ""
    chars  = {0:'Ga', 1:'Bu', 2:'Zo', 3:'Meu'}
    while n > 0:
        r = chars[n%4] + r
        n = n//4
    return r

def shadokToDec(n):
    n = n.replace('Ga', '0').replace('Bu', '1').replace('Zo', '2').replace('Meu', '3')
    chars = {'0':0, '1':1, '2':2, '3':3}
    r = 0
    i = len(n)
    p = 0
    while i > 0:
        i -= 1
        r += chars[n[i]]*4**p
        p += 1
    return r

def decToBibi(n):
    r     = ''
    chars = {0:'Ho', 1:'Ha', 2:'He', 3:'Hi', 4:'Bo', 5:'Ba', 6:'Be', 7:'Bi', 8:'Ko', 9:'Ka', 10:'Ke', 11:'Ki', 12:'Do', 13:'Da', 14:'De', 15:'Di'}
    while n > 0:
        r = chars[n%16] + r
        n = n//16
    return r

def bibiToDec(n):
    hex = '0123456789abcdef'
    j   = 0
    for i in ['Ho', 'Ha', 'He', 'Hi', 'Bo', 'Ba', 'Be', 'Bi', 'Ko', 'Ka', 'Ke', 'Ki', 'Do', 'Da', 'De', 'Di']:
        n = n.replace(i, hex[j])
        j += 1

    return hexToDec(n)
